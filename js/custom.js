$(document).ready(function() {
    ///////////////////////////////////////////////////////
    // FOCUS EVENTS FOR INPUT FIELDS //////////////////////
    ///////////////////////////////////////////////////////
    
    // need to add a data-attr to save the default value
    $(".clearFocus").each(function() {
        $(this).attr("data-default", $(this).val());
    });

    // if the current value is = default, clear
    $(".clearFocus").on("focus", function () {
        if ($(this).val() == $(this).data("default")){
            $(this).val("");
        }
    });

    // if value is empty, restore to default
    $(".clearFocus").on("focusout", function() {
        if ($(this).val() == "") {
            $(this).val($(this).data("default"));
        }
    });


    ///////////////////////////////////////////////////////
    // CONTROLS FOR TESTIMONIAL WIDGET ////////////////////
    ///////////////////////////////////////////////////////
 
	$("#test-less i").on("click", function () {
		var id = $("#test-num").text();
		var load_id = parseInt(id) - 1;
		loadTestimonial(load_id);
	});

	$("#test-more i").on("click", function () {
		var id = $("#test-num").text();
		var load_id = parseInt(id) + 1;
		loadTestimonial(load_id);
	});
});

function loadTestimonial(id) {
	var testimonials = {
			
	};
	testimonials.count = 3; // heh

	if (id > 0 && id <= testimonials.count) {
		alert ("TODO: load testimonial " + id + "; ajax or hidden like via slides");
		$("#test-num").text(id);
	} else {
		alert("Testimonial non-existent");
	}
}
